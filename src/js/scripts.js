"use strict"

let menu = document.querySelector(".burger-toggle");
let menuList = document.querySelector(".nav-list");

menu.addEventListener('mousedown', elem => {
    if (elem.currentTarget.classList.contains('burger-toggle_active')) {
        menuList.style.display = "none";
        elem.currentTarget.classList.remove("burger-toggle_active");
    } else {
        menuList.style.display = "block";
        elem.currentTarget.classList.add("burger-toggle_active");
    }
})
