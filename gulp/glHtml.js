const gulp = require('gulp');
const htmlValidator = require('gulp-w3c-html-validator');
const plumber = require('gulp-plumber');
const pipeline = require('readable-stream').pipeline;


module.exports = function glHtml() {
    return pipeline(
        gulp.src('*.html'),
        plumber(),
        htmlValidator(),
        gulp.dest('public'));
};
