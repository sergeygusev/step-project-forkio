const gulp = require('gulp');
const imagemin = require('gulp-imagemin');

module.exports = function glImages() {
    return gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/images'))
}