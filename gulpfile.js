const gulp = require('gulp');

const serve = require('./gulp/serve');
const html = require('./gulp/html');
const styles = require('./gulp/styles');
const js = require('./gulp/js');
const clean = require('./gulp/clean');
const images = require('./gulp/images');
const glImages = require('./gulp/glImages');
const glClean = require('./gulp/glClean');
const glStyles = require('./gulp/glStyles');
const glJs = require('./gulp/glJs');
const glHtml = require('./gulp/glHtml');

const dev = gulp.parallel(html, js, styles);
const build = gulp.series(clean, images, styles, js, html);
const gitlab = gulp.series(glClean, glImages, glStyles, glJs, glHtml);

module.exports.dev = gulp.series(dev, serve);
module.exports.build = gulp.series(build);
module.exports.gitlab = gulp.series(gitlab);
