## What been used?

Full list of the node modules packages you may find in file named "package.json".

## Developers

### Sergey Gusev
Responsible for:
1. Header section,
2. People are talking section,
3. Posting project on Gitlub Pages.

### Olya Kovaliuk
Responsible for:
1. Revolutionary editor section,
2. What you get section.

### Command work:
1. Building project by gulp task manager,
2. Pricing section,
3. Merging branches in a remote repository.
